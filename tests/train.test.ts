import { describe, expect, it } from "vitest";
import type { Event } from "~/src/clock.ts";
import { Clock } from "~/src/clock.ts";
import { Track } from "~/src/track.ts";
import {
    AccelerationCompleteEvent,
    PassesTrackItemEvent,
    pzb,
    Train,
    TrainExceedsSupervisedVelocityEvent,
} from "~/src/train.ts";

const expectNextEvent = <E extends Event>(clock: Clock, clazz: { new(...args: any[]): E }): E => {
    const event = clock.simulateUntilNextEvent();
    expect(event).toBeInstanceOf(clazz);
    return event as unknown as E;
};

describe("train movement", () => {
    const createFixture = () => {
        const item = {
            position: 100,
            description: "generic track item",
            handleTrainPassing(): void {
            },
        };
        const track = new Track(1000, [item]);
        const train = new Train(0, 100, 0, 0, track);
        const clock = new Clock([train]);
        return { item, train, clock };
    };

    it("never reaches a track item when stopped", () => {
        const { clock } = createFixture();
        const event = clock.simulateUntilNextEvent();
        expect(event).toBeUndefined();
    });
    it("reaches a track item when driving at constant speed", () => {
        const { item, train, clock } = createFixture();
        train.velocity = 10;

        const event = expectNextEvent(clock, PassesTrackItemEvent);
        expect(event.time).toBeCloseTo(10, 1);
    });
    it("reaches a track item when accelerating", () => {
        const { item, train, clock } = createFixture();
        train.acceleration = 2;

        const event = clock.simulateUntilNextEvent();
        expect(event).toBeInstanceOf(PassesTrackItemEvent);
        const passesTrackItemEvent = event as PassesTrackItemEvent;
        expect(passesTrackItemEvent.time).toBeCloseTo(10, 1);
    });
});

describe("train protection system PZB", () => {
    const createFixture = () => {
        const track = new Track(1000, []);
        const train = new Train(0, 100, 120 / 3.6, 0, track);
        const clock = new Clock([train, train.trainProtectionSystem]);
        return { train, clock };
    };

    describe("a 2000 Hz interaction", () => {
        it("breaks to a halt", () => {
            const { train } = createFixture();
            train.trainProtectionSystem.handleMagnetInteraction(2000, 0);
            expect(train.acceleration).toBe(-1);
        });
    });

    describe("a 1000 Hz interaction", () => {
        it("breaks to a halt when exceeding braking curve at constant speed", () => {
            const { train, clock } = createFixture();
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            const time = (pzb.maxVelocity - train.velocity) / pzb.cautionAcceleration;
            const event = expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expect(event.time).toBeCloseTo(time);
            expect(train.acceleration).toBe(-1);
        });
        it("breaks to a halt when exceeding braking curve while accelerating", () => {
            const { train, clock } = createFixture();
            train.velocity = 60 / 3.6;
            train.acceleration = 1;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            const time = (pzb.maxVelocity - train.velocity) / (train.acceleration + pzb.cautionAcceleration);
            const event = expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expect(event.time).toBeCloseTo(time, 1);
            expect(train.acceleration).toBe(-1);
        });
        it("allows movement when breaking exactly at supervised breaking curve", () => {
            const { train, clock } = createFixture();
            train.acceleration = -pzb.cautionAcceleration;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            const event = expectNextEvent(clock, AccelerationCompleteEvent);
            expect(event.type).toBe("stop");
        });
        it("allows movement when breaking stronger than supervised breaking curve", () => {
            const { train, clock } = createFixture();
            train.acceleration = -1;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            const event = expectNextEvent(clock, AccelerationCompleteEvent);
            expect(event.type).toBe("stop");
        });
        it("allows travel of release speed", () => {
            const { train, clock } = createFixture();
            train.velocity = pzb.cautionVelocity;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            const event = clock.simulateUntilNextEvent();
            expect(event).toBeUndefined();
        });
        it("breaks to halt when exceeding release speed", () => {
            const { train, clock } = createFixture();
            train.velocity = 80 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            clock.updateTime(pzb.cautionDuration, 0);
            train.acceleration = 1 / 3.6;

            const event = expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expect(event.time).toBeCloseTo(pzb.cautionDuration + 5, 1);
            expect(train.acceleration).toBe(-1);
        });
        it("discontinues release speed after supervision length", () => {
            const { train, clock } = createFixture();
            train.velocity = 80 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            clock.updateTime(pzb.cautionLength / train.velocity - 4, 0);
            train.acceleration = 1 / 3.6;

            const event = expectNextEvent(clock, AccelerationCompleteEvent);
            expect(event.type).toBe("max-speed");
        });
        it("breaks to halt again when exceeding supervised velocity after halt", () => {
            const { train, clock } = createFixture();
            train.velocity = 90 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(1000, 0);

            expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expectNextEvent(clock, AccelerationCompleteEvent);

            train.acceleration = 1;

            expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
        });
    });

    describe("a 500 Hz interaction", () => {
        it("breaks to a halt when exceeding breaking cure at constant speed", () => {
            const { train, clock } = createFixture();
            train.velocity = 60 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(500, 0);

            const time = (train.velocity - pzb.nearStartVelocity) / train.velocity / -pzb.nearTransition;
            const event = expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expect(event.time).toBeCloseTo(time, 1);
            expect(train.acceleration).toBe(-1);
        });
        it("breaks to a halt when exceeding breaking curve at constant speed higher than start velocity", () => {
            const { train, clock } = createFixture();
            train.velocity = 75 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(500, 0);

            const event = expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expect(event.time).toBeCloseTo(0, 1);
            expect(train.acceleration).toBe(-1);
        });
        it("allows travel of release speed", () => {
            const { train, clock } = createFixture();
            train.velocity = pzb.nearReleaseVelocity;
            train.trainProtectionSystem.handleMagnetInteraction(500, 0);

            const event = clock.simulateUntilNextEvent();
            expect(event).toBeUndefined();
        });
        it("breaks to halt when exceeding release speed", () => {
            const { train, clock } = createFixture();
            train.velocity = 40 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(500, 0);

            clock.updateTime(pzb.nearTransitionLength / train.velocity, 0);
            train.acceleration = 1 / 3.6;

            expectNextEvent(clock, TrainExceedsSupervisedVelocityEvent);
            expect(train.acceleration).toBe(-1);
        });
        it("discontinues release speed after supervision length", () => {
            const { train, clock } = createFixture();
            train.velocity = 40 / 3.6;
            train.trainProtectionSystem.handleMagnetInteraction(500, 0);

            clock.updateTime(pzb.nearLength / train.velocity - 4, 0);
            train.acceleration = 1 / 3.6;

            const event = expectNextEvent(clock, AccelerationCompleteEvent);
            expect(event.type).toBe("max-speed");
        });
    });
});

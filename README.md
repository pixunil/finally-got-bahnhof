# Finally got Bahnhof

Little animations describing the workings of German railway operation.

## Dependencies

- [Node 22](https://nodejs.org/)
- [Pnpm 9](https://pnpm.io/)
- [Nuxt 3](https://nuxt.com/)

## Setup

```bash
pnpm install
```

## Run

### Development

Start the development server on `http://localhost:3000`:

```bash
pnpm run dev
```

### Production

Build the application for production:

```bash
pnpm run build
```

Locally preview production build:

```bash
pnpm run preview
```

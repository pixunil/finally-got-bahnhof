export default defineNuxtConfig({
    devtools: { enabled: true },
    modules: [
        "@nuxt/test-utils/module",
    ],
    css: [
        "~/assets/style.scss",
    ],
    typescript: {
        tsConfig: {
            compilerOptions: {
                allowImportingTsExtensions: true,
            },
        },
    },
});

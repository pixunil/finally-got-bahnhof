import { ChangeAccelerationEvent } from "~/src/train.ts";

export class Clock {
    currentTime: Ref<number>;
    ticks: number;
    restTime: number;
    tickDuration: number;
    ticksPerStep: number;
    timed: Timed[];
    saveCommand: SaveCommand;
    commands: Ref<ChangeAccelerationEvent[]>;
    simulationEvents: Event[];

    constructor(timed: Timed[]) {
        this.currentTime = ref(0);
        this.ticks = 0;
        this.restTime = 0;
        this.tickDuration = 0.01;
        this.ticksPerStep = 10;
        this.timed = timed;
        this.saveCommand = new SaveCommand(this);
        this.commands = ref([]);
        this.simulationEvents = [];
        for (const timedEntity of this.getTimed()) {
            timedEntity.subscribers.push(event => this.saveCommand.addEvent(event));
        }

        watch(this.currentTime, (newTime, oldTime) => this.updateTime(newTime, oldTime));
    }

    get stepDuration(): number {
        return this.tickDuration * this.ticksPerStep;
    }

    addCommand(command: ChangeAccelerationEvent) {
        this.commands.value.push(command);
        if (this.currentTime.value > command.time) {
            this.currentTime.value = command.time;
        }
    }

    removeCommand(command: ChangeAccelerationEvent) {
        this.commands.value = this.commands.value.filter(existingCommand => !Object.is(existingCommand, command));
        if (this.currentTime.value > command.time) {
            this.currentTime.value = command.time - 1;
        }
    }

    getTimed(scope?: Scope): Timed[] {
        if (scope === "simulation") {
            return [...this.timed, ...this.commands.value];
        } else if (scope === "recording") {
            return [this.saveCommand];
        } else {
            return [...this.timed, ...this.commands.value, this.saveCommand];
        }
    }

    updateTime(newTime: number, oldTime: number) {
        if (newTime < oldTime) {
            const rewindedStep = Math.max(Math.floor(newTime / this.stepDuration), 0);
            const rewindedTime = rewindedStep * this.stepDuration;
            for (const timed of this.getTimed()) {
                timed.reset(rewindedStep, rewindedTime);
            }
            this.ticks = rewindedStep * this.ticksPerStep;
            this.restTime = 0;
            oldTime = rewindedTime;
        }

        const timeDifference = newTime - oldTime + this.restTime;
        const ticks = Math.floor(timeDifference / this.tickDuration);

        for (let i = 0; i < ticks; i++) {
            this.simulateTick();
        }

        this.restTime = newTime - this.ticks * this.tickDuration;
    }

    simulateUntilNextEvent(maxDuration: number = 100): Event | undefined {
        const maxTicks = Math.floor(maxDuration / this.tickDuration);
        const previousEventCount = this.saveCommand.events.value.length;
        for (let tick = 0; tick < maxTicks; tick++) {
            this.simulateTick();
            if (this.saveCommand.events.value.length > previousEventCount) {
                return this.saveCommand.events.value[previousEventCount];
            }
        }
    }

    simulateTick() {
        this.ticks++;
        let currentTime = this.ticks * this.tickDuration;
        for (const timed of this.getTimed("simulation")) {
            timed.update(this.tickDuration, currentTime);
        }
        if (this.ticks % this.ticksPerStep === 0) {
            this.saveCommand.saveAll();
        }
    }
}

type Scope = "simulation" | "recording";

export interface Event {
    readonly time: number;
    description: string;
    userDefined?: boolean;
}

export abstract class Timed {
    subscribers: ((event: Event) => void)[] = [];

    protected emit(event: Event) {
        for (const subscriber of this.subscribers) {
            subscriber(event);
        }
    }

    update(timePassed: number, currentTime: number) {
    }

    save(): void {
    }

    reset(rewindedStep: number, rewindedTime: number): void {
    }
}

class SaveCommand extends Timed {
    steps: number;
    clock: Clock;
    events: Ref<Event[]>;

    constructor(clock: Clock) {
        super();
        this.steps = 0;
        this.clock = clock;
        this.events = ref([]);
    }

    saveAll() {
        for (const timed of this.clock.timed) {
            timed.save();
        }
        this.steps++;
    }

    addEvent(event: Event) {
        this.events.value.push(event);
    }

    override reset(rewindedStep: number, rewindedTime: number): void {
        this.steps = rewindedStep;
        this.events.value = this.events.value.filter(event => event.time <= rewindedTime);
    }
}

import type { Ref, UnwrapRef } from "vue";
import type { Event } from "~/src/clock.ts";
import { Timed } from "~/src/clock.ts";
import type { Frequency } from "~/src/magnet.ts";
import type { Track, TrackItem } from "~/src/track.ts";

export class AccelerationCompleteEvent implements Event {
    time: number;
    train: Train;
    type: "stop" | "max-speed";

    constructor(time: number, train: Train, type: "stop" | "max-speed") {
        this.time = time;
        this.train = train;
        this.type = type;
    }

    get description() {
        if (this.type === "stop") {
            return "Zug kommt zum Stillstand";
        } else {
            return "Zug erreicht Höchstgeschwindigkeit";
        }
    }
}

export class PassesTrackItemEvent implements Event {
    time: number;
    train: Train;
    item: TrackItem;

    constructor(time: number, train: Train, item: TrackItem) {
        this.time = time;
        this.train = train;
        this.item = item;
    }

    get description() {
        return `Zug passiert ${this.item.description} an ${(this.item.position / 1000).toFixed(1)} km`;
    }
}

export class Train extends Timed {
    positionFront: Ref<number>;
    length: number;
    velocity: number;
    maxVelocity: number;
    acceleration: number;
    trainProtectionSystem: TrainProtectionSystem;
    track: Track;
    trackSegment: number;
    history: Ref<UnwrapRef<Pick<Train, "positionFront" | "velocity" | "acceleration" | "trackSegment">>[]>;

    constructor(positionFront: number, length: number, velocity: number, acceleration: number, track: Track) {
        super();
        this.positionFront = ref(positionFront);
        this.length = length;
        this.velocity = velocity;
        this.maxVelocity = 120 / 3.6;
        this.acceleration = acceleration;
        this.trainProtectionSystem = new TrainProtectionSystem(this);
        this.track = track;
        this.trackSegment = 0;
        this.history = ref([]);
        this.save();
    }

    checkAccelerationComplete(currentTime: number) {
        if (this.acceleration < 0 && this.velocity <= 0) {
            this.emit(new AccelerationCompleteEvent(currentTime, this, "stop"));
            this.acceleration = 0;
            this.trainProtectionSystem.breaksToHalt = false;
        } else if (this.acceleration > 0 && this.velocity >= this.maxVelocity) {
            this.emit(new AccelerationCompleteEvent(currentTime, this, "max-speed"));
            this.acceleration = 0;
        }
    }

    checkPassesTrackItems(currentTime: number) {
        for (const trackItem of this.track.items.slice(this.trackSegment)) {
            if (this.positionFront.value >= trackItem.position) {
                let event = new PassesTrackItemEvent(currentTime, this, trackItem);
                this.emit(event);
                trackItem.handleTrainPassing(this, currentTime);
                this.trackSegment++;
            }
        }
    }

    override update(timePassed: number, currentTime: number) {
        const previousPositionFront = this.positionFront.value;
        this.positionFront.value += 0.5 * this.acceleration * timePassed ** 2 + this.velocity * timePassed;
        this.velocity += this.acceleration * timePassed;
        this.checkAccelerationComplete(currentTime);
        this.checkPassesTrackItems(currentTime);
    }

    override save() {
        this.history.value.push({
            positionFront: this.positionFront.value,
            velocity: this.velocity,
            acceleration: this.acceleration,
            trackSegment: this.trackSegment,
        });
    }

    override reset(rewindedStep: number) {
        const rewindedState = this.history.value[rewindedStep];
        this.positionFront.value = rewindedState.positionFront;
        this.velocity = rewindedState.velocity;
        this.acceleration = rewindedState.acceleration;
        this.trackSegment = rewindedState.trackSegment;
        this.history.value.splice(rewindedStep + 1, this.history.value.length - rewindedStep - 1);
    }
}

export class TrainExceedsSupervisedVelocityEvent implements Event {
    time: number;
    trainProtectionSystem: TrainProtectionSystem;
    description = "Zug überschreitet überwachte Höchstgeschwindigkeit und wird zwangsgebremst";

    constructor(time: number, trainProtectionSystem: TrainProtectionSystem) {
        this.time = time;
        this.trainProtectionSystem = trainProtectionSystem;
    }
}

class PzbValues {
    maxVelocity: number;
    cautionVelocity: number;
    cautionDuration: number;
    cautionLength: number;
    nearStartVelocity: number;
    nearReleaseVelocity: number;
    nearTransitionLength: number;
    nearLength: number;

    constructor(maxVelocity: number, cautionVelocity: number, cautionDuration: number, nearStartVelocity: number, nearReleaseVelocity: number) {
        this.maxVelocity = maxVelocity;
        this.cautionVelocity = cautionVelocity;
        this.cautionDuration = cautionDuration;
        this.cautionLength = 1250;
        this.nearStartVelocity = nearStartVelocity;
        this.nearReleaseVelocity = nearReleaseVelocity;
        this.nearTransitionLength = 153;
        this.nearLength = 250;
    }

    get cautionAcceleration(): number {
        return (this.maxVelocity - this.cautionVelocity) / this.cautionDuration;
    }

    get nearTransition(): number {
        return (this.nearStartVelocity - this.nearReleaseVelocity) / this.nearTransitionLength;
    }
}

export const pzb = new PzbValues(165 / 3.6, 85 / 3.6, 23, 65 / 3.6, 45 / 3.6);

class TrainProtectionSystem extends Timed {
    train: Train;
    cautionInteraction: { time: number, position: number } | null;
    nearInteraction: { time: number, position: number } | null;
    supervisedVelocity: number | null;
    breaksToHalt: boolean;
    history: Ref<Pick<TrainProtectionSystem, "cautionInteraction" | "nearInteraction" | "supervisedVelocity" | "breaksToHalt">[]>;

    constructor(train: Train) {
        super();
        this.train = train;
        this.cautionInteraction = null;
        this.nearInteraction = null;
        this.supervisedVelocity = null;
        this.breaksToHalt = false;
        this.history = ref([]);
        this.save();
    }

    handleMagnetInteraction(frequency: Frequency, currentTime: number): void {
        switch (frequency) {
            case 1000:
                this.cautionInteraction = { time: currentTime, position: this.train.positionFront.value };
                break;
            case 500:
                this.nearInteraction = { time: currentTime, position: this.train.positionFront.value };
                break;
            case 2000:
                this.train.acceleration = -1;
                break;
        }
    }

    breakToHalt(): void {
        this.train.acceleration = -1;
        this.breaksToHalt = true;
    }

    override update(timePassed: number, currentTime: number): void {
        this.supervisedVelocity = null;
        if (this.cautionInteraction !== null) {
            if (this.cautionInteraction.position + pzb.cautionLength < this.train.positionFront.value) {
                this.cautionInteraction = null;
            } else {
                this.supervisedVelocity = Math.max(pzb.maxVelocity - (currentTime - this.cautionInteraction.time) * pzb.cautionAcceleration,
                    pzb.cautionVelocity);
                
            }
        }
        if (this.nearInteraction !== null) {
            if (this.nearInteraction.position + pzb.nearLength < this.train.positionFront.value) {
                this.nearInteraction = null;
            } else {
                this.supervisedVelocity = Math.max(pzb.nearStartVelocity - (this.train.positionFront.value - this.nearInteraction.position) * pzb.nearTransition,
                    pzb.nearReleaseVelocity);
            }
        }
        if (this.supervisedVelocity !== null && this.train.velocity > this.supervisedVelocity && !this.breaksToHalt) {
            this.emit(new TrainExceedsSupervisedVelocityEvent(currentTime, this));
            this.breakToHalt();
        }
    }

    override save(): void {
        this.history.value.push({
            cautionInteraction: this.cautionInteraction,
            nearInteraction: this.nearInteraction,
            supervisedVelocity: this.supervisedVelocity,
            breaksToHalt: this.breaksToHalt,
        });
    }

    override reset(rewindedStep: number, rewindedTime: number): void {
        const rewindedState = this.history.value[rewindedStep];
        this.cautionInteraction = rewindedState.cautionInteraction;
        this.nearInteraction = rewindedState.nearInteraction;
        this.supervisedVelocity = rewindedState.supervisedVelocity;
        this.breaksToHalt = rewindedState.breaksToHalt;
        this.history.value.splice(rewindedStep + 1, this.history.value.length - rewindedStep - 1);
    }
}

export class ChangeAccelerationEvent extends Timed implements Event {
    time: number;
    userDefined = true;
    triggered: boolean;
    train: Train;
    newAcceleration: number;

    constructor(triggerTime: number, train: Train, newAcceleration: number) {
        super();
        this.time = triggerTime;
        this.triggered = false;
        this.train = train;
        this.newAcceleration = newAcceleration;
    }

    get description() {
        if (this.newAcceleration === 0) {
            return "Zug hält Geschwindigkeit";
        } else if (this.newAcceleration < 0) {
            return `Zug bremst mit ${-this.newAcceleration} m/s²`;
        } else {
            return `Zug beschleunigt mit ${this.newAcceleration} m/s²`;
        }
    }

    override update(timePassed: number, currentTime: number): void {
        if (currentTime > this.time && !this.triggered) {
            this.emit(this);
            this.train.acceleration = this.newAcceleration;
            this.triggered = true;
        }
    }

    override reset(rewindedStep: number, rewindedTime: number): void {
        if (rewindedTime <= this.time) {
            this.triggered = false;
        }
    }
}

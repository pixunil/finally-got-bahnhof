import type { TrackItem } from "~/src/track.ts";
import type { Train } from "~/src/train.ts";

export type Frequency = 500 | 1000 | 2000;

export class Magnet implements TrackItem {
    position: number;
    frequency: Frequency;

    constructor(position: number, frequency: Frequency) {
        this.position = position;
        this.frequency = frequency;
    }

    get description() {
        return `${this.frequency} Hz Magnet`;
    }

    handleTrainPassing(train: Train, currentTime: number): void {
        train.trainProtectionSystem.handleMagnetInteraction(this.frequency, currentTime);
    }
}

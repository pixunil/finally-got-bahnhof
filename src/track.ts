import { Train } from "~/src/train.ts";

export class Track {
    length: number;
    items: TrackItem[];


    constructor(length: number, items: TrackItem[]) {
        this.length = length;
        this.items = items.toSorted((a, b) => a.position - b.position);
    }

    getNextItem(position: number): [TrackItem, number] | null {
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].position >= position) {
                return [this.items[i], i];
            }
        }
        return null;
    }

    getDistanceToNextItem(position: number): number | null {
        const nextItem = this.getNextItem(position);
        if (nextItem === null) {
            return null;
        }
        return nextItem[0].position - position;
    }
}

export interface TrackItem {
    position: number;
    description: string;

    handleTrainPassing(train: Train, currentTime: number): void;
}
